package function;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.Timer;

public class Clock extends Thread{
	private static final SimpleDateFormat df =
			new SimpleDateFormat("HH");
	private static final SimpleDateFormat df2 =
			new SimpleDateFormat("mm");
	
	private final static Date now = new Date();
	private final static Timer timer = new Timer(1000,null);


	public Clock() {
		timer.start();
    	//System.out.println("clock");
	}
	public String stringClock(int str){
		String s1 = df.format(now);
		String s2 = " - - ";
		String s3 = "  "+df2.format(now);
		now.setTime(System.currentTimeMillis());
    	//System.out.println("stringc");
		if(str==0){
			return s1;
		}
		else if(str==1){
			return s2;
		}
		else{
		return s3;
		
		}
	}
}