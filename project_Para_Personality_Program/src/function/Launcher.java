package function;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import project_Para_Personality_Program.Main;

public class Launcher extends Thread {

	public static JFrame fl = new JFrame();
	public static JPanel pl = new JPanel();
	private static int fli = 0;
	private static boolean firstOn = true;
	public static int bi = 0;
	
	//ファイルネームとファイルパスの一時保管ArrayList
	//ファイルネームが偶数（０を含む）でファイルパスが奇数
	//2i と 2i+1 で一つのファイルを指定する
	private ArrayList<String> tempFileArray = new ArrayList<>();
	
	public static boolean addaction;
	private Getpath gp = new Getpath();
	
	public static JButton[] bt = new JButton[100];
	JButton btadd = new JButton("Add");
	JButton btDelete = new JButton("Delete");
	JButton btClose = new JButton("Close");

	public Launcher() {
		
		if(firstOn){
			//Launcherで表示するファイルのArrayListを読み込む
			tempFileArray = Main.save.loadLauncherList();
			firstOn = false;
		}
		
		DropTarget dtarget = new DropTarget(fl, new Getpath());
		
		btadd.setContentAreaFilled(false);//背景の透明化
		btadd.setForeground(Color.white);//文字色
		
		btDelete.setContentAreaFilled(false);
		btDelete.setForeground(Color.white);
		
		btClose.setContentAreaFilled(false);
		btClose.setForeground(Color.white);
		
		btadd.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				addaction=true;
				gp.filename(addaction,bi);
				
				System.out.println("addend");
			}
		});
		
		tempFileArray.add(Getpath.filename);
		tempFileArray.add(Getpath.path);
		
		
		//tempFileArray.get(tempFileArray.size() - 2)  →  Getpath.filename
		//tempFileArray.get(tempFileArray.size() - 1)  →  Getpath.path
		
		
		//Addボタンがクリックされた時だけボタンが追加されるようにする
		//これがないと何もしないでLauncherを開いて閉じてを繰り返すだけで同じボタンが増える
		if(addaction){
			
			bt[bi] = new JButton(tempFileArray.get(tempFileArray.size() - 2));	//最後に追加したファイルの名前を呼びだす
			bt[bi].setContentAreaFilled(false);
			bt[bi].setForeground(Color.white);
			bt[bi].addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					try {
						Desktop dt = Desktop.getDesktop();
						File fpath = new File(tempFileArray.get(tempFileArray.size() - 1));		//最後に追加したファイルへのパスを呼び出す
						dt.open(fpath);
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			});
			System.out.println(Getpath.filename);
			if(tempFileArray.get(tempFileArray.size() - 2) != null){
				
				//複数のボタンを追加する処理はここか？
				pl.add(bt[bi]);
				System.out.println("addbt");
			}
			bi ++;
			addaction = false;
			
			//追加されたファイルのデータを保存
			//Saveオブジェクトは何度も生成してはいけない
			//生成の瞬間にコンストラクタによって起動時間が保存されるのでごちゃごちゃになるからだ
			//またデータが共通のものじゃなければ意味がないのも理由の一つである
			Main.save.saveLauncherList(tempFileArray);
			
		}
		
		btClose.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				fl.dispose();
    			Main.pt = true;
				}
		});
		
		if(fli == 1){
			pl.add(btadd);
			pl.add(btDelete);
			pl.add(btClose);
		}
		pl.setBackground(new Color(0.2f,0,0f,0.2f));
		
		fl.getContentPane().add(pl, BorderLayout.CENTER);
	}
	
	static void createfl(int w,int h) {
		fl.setBounds(w,h,500,300);
		fl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fl.setUndecorated(true);
		fl.setBackground(new Color(0.2f, 0f, 0f, 0.5f));//赤・緑・青・透明度
		//fl.pack();
		fl.setVisible(true);
		fl.setAlwaysOnTop(true);
	}
	public static void run(int w,int h) {
		fli++;
		createfl(w,h);
	}
}