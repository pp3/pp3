package function;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class CalendarBeta extends Thread {

	

	public Calendar calendar = Calendar.getInstance();
	
	public String[] day_Name = {"日","月","火","水","木","金","土"};		//Calenderクラスの曜日は日曜日から始まる
	//またCalendarクラスのgetメソッドの戻り値（整数、日曜日が1）によって文字を出力するようにするためのもの
	
	//曜日と同じ使用目的。使うかは不明
	public String[] month_Name = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	
	public int[] calendarDate;
	public JButton[] calendarButton;
	
	
	
	//各種値の読み込み
	public int year = calendar.get(Calendar.YEAR);
	public int month = calendar.get(Calendar.MONTH) + 1;			//一月がデフォルトで０に設定されている
	public int date = calendar.get(Calendar.DATE);
	public int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;	//日曜日が０になるように調整
	public int am_Pm = calendar.get(Calendar.AM_PM);
	
	//基本的なデータはを格納するint型配列
	public int[] cal_data = {year, month, date, am_Pm};
	
	//月の初日の曜日と月末の日にちを格納する配列
	public int[] cal_Data = new int[2];
	
	//年、月、日、時間帯（午前か午後か）をint型配列に格納するコンストラクタ
	//曜日は文字で返したかったので別にした
	//ボツ、いらない
	public void Calendar(){
		
	}
	

	//以下の～Returnメソッドはswitch文を使用したり、かっこが重複することを避けるために設けている
	//本当に正しいかどうかはわからない
	//法律と同じなのよ（遠い目）
	
	//現在の曜日を文字で返すメソッド
	public String dayReturn(){
		
		return day_Name[day];
	}
	
	
	//現在の月を返すメソッド
	public String monthReturn(){
		
		return month_Name[month - 1];
	}
	

	
	
	//その月の初日の曜日と最大日数を返すメソッド
	
	public int[] monthMaxReturn(int mth){
		
		//月の初日の曜日を求め、格納
		calendar.set(year, mth, 1);							//今月の初日にセット
		cal_Data[0] = calendar.get(Calendar.DAY_OF_WEEK);	//その曜日をゲット（韻を踏んだ）
		
		//その月の最終日を求め、格納
		calendar.add(Calendar.MONTH, 1);					//今月の一日の状態から一ヶ月後にカレンダーをセット
		calendar.add(Calendar.DAY_OF_MONTH, -1);			//さらにそこから一日前に日付をセット
		cal_Data[1] = calendar.get(Calendar.DATE);			//その日をゲット（韻を（ｒｙ）
		
		return cal_Data;
	}
	
	
	
	//日付を配列の中に実際のカレンダー見たく入れるメソッド
	public void dateEnter(){
		
		int i;
		

		//初日より前は0を格納しておく
		for(i = 0; i < cal_Data[0] - 1; i++){
			calendarDate[i] = 0;
		}
		
		//i番目から順に日付を格納していく
		for(int j = i; j < cal_Data[1] + i; j++){
			calendarDate[j] = (j - i + 1);		// j - i + １ で1から順にインクリメントされる
		}
		
	}
	
	
	//カレンダーを描画するメソッド
	//日にち一個一個をボタンにすることで将来タスク管理プログラムを入れる時に活用できるようにする
	public void paintCalendar(){
		
		JPanel calendarPanel = new JPanel();
		
		
		
		calendarPanel.setLayout(null);
		
		for(int i = 0; i < cal_Data[1]; i++){
			if(calendarDate[i] != 0){
				
				final int si = i;
				
				calendarButton[i] = new JButton(String.valueOf(calendarDate[i]));
				calendarButton[i].setContentAreaFilled(false);
				calendarButton[i].setForeground(Color.white);
				calendarButton[i].addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						System.out.println(calendarDate[si]);
					}
				});
				
				calendarButton[i].setBounds(50 * (i % 7), 70 * (i / 7), 50, 70);
				
			}
		}
		
	}
		
		public void createCalFrame() {
			
			JFrame fl = new JFrame();
			
			fl.setBounds(0, 0, 500, 300);
			fl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fl.setUndecorated(true);
			fl.setBackground(new Color(0.2f, 0.2f, 0f, 0.5f));//赤・緑・青・透明度
			//fl.pack();
			fl.setVisible(true);
			fl.setAlwaysOnTop(true);
		}
		
		
		public void run(){
			createCalFrame();
			paintCalendar();
		}
		
	
}
