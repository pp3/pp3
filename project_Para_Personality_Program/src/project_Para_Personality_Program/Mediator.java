package project_Para_Personality_Program;

import java.util.Random;


public class Mediator extends Thread{
	
	private ImageControl ImgC;
	private int count = 0, num = 0, num_back = 0;
	private boolean numFlag = true, numFlag_back = true;		//インクリメントとデクリメントの切り替えフラグ
	//trueならインクリメント
	//falseならデクリメント
	
	
	private Random rnd = new Random();
	
	
	public Mediator(){
		ImgC = new ImageControl();
	}
	
	
	public Mediator(ImageControl ic){
		ImgC = ic;
	}
	
	//numのインクリメントとデクリメントの切り替えメソッド
	public void numSwitch(){
		num += (numFlag) ? 1 : (-1);		//三項演算子を使用
	}
	
	public void numSwitch_back(){
	num_back += (numFlag_back) ? 1 : (-1);
	//num_back = rnd.nextInt(4);
	}
	
	public void run(){
		
		while(true){
			
			if(count % 8 == 0){	//13
				
				numSwitch();
				ImgC.catchImage(num);
				
				if(num ==2 && numFlag){		//numが2以上になってしかもインクリメント途中なら
					numFlag = false;
				}else if(num == 0 && !numFlag){
					numFlag = true;
				}
			}
			
				numSwitch_back();
				ImgC.catchImageBack(num_back);
				
				if(num_back ==10 && numFlag_back){		//num_backが10以上になってしかもインクリメント途中なら
					numFlag_back = false;
				}else if(num_back== 0 && !numFlag_back){
					numFlag_back = true;
				}

			
			//System.out.println("wtf");
			
			try{
		        Thread.sleep(80);
		      }catch(InterruptedException e){
		      }
			finally{
				count++;
			}
			
		}
		
	}
}
