package project_Para_Personality_Program;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import function.Launcher;

public class FrameButton extends Thread {

	static JFrame fb = new JFrame();
	private static JPanel p = new JPanel();
	private static int fbi = 0;		//ボタン設置のカウント

	//各種ボタンの変数名はbt役割に統一
	private static final JToggleButton btClock = new JToggleButton("Clock");		//on/off切り替えのためトグル
	JButton btWeather = new JButton("Weather");
	JButton btExit = new JButton("Exit");
	JButton btCloseMenu = new JButton("CloseMenu");
	JButton btSetting = new JButton("Setting");
	JButton btLaunchar = new JButton("Launcher");

	FrameButton(final int w,final int h) {
		//背景色と文字色の設定
		getButton1().setContentAreaFilled(false);
		getButton1().setForeground(Color.white);

		btWeather.setContentAreaFilled(false);
		btWeather.setForeground(Color.white);

		btExit.setContentAreaFilled(false);
		btExit.setForeground(Color.white);

		btCloseMenu.setContentAreaFilled(false);
		btCloseMenu.setForeground(Color.white);

		btSetting.setContentAreaFilled(false);
		btSetting.setForeground(Color.white);

		btLaunchar.setContentAreaFilled(false);
		btLaunchar.setForeground(Color.white);

		//ボタンが押されたときの反応
		btWeather.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
			}
		});

		btExit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(0);
			}
		});

		btCloseMenu.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				fb.dispose();
				Launcher.fl.dispose();
    			Main.pt = true;
			}
		});

		btLaunchar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Launcher.run(w,h);
    			Launcher fl = new Launcher();
    			fl.start();
			}
		});

		//初回のみボタンを設置
		if(fbi == 1){
			p.add(getButton1());
			p.add(btWeather);
			p.add(btLaunchar);
			p.add(btSetting);
			p.add(btExit);
			p.add(btCloseMenu);
		}
		p.setBackground(new Color(0,0,0.2f,0.2f));
		fb.getContentPane().add(p, BorderLayout.CENTER);
    	//System.out.println("fb");
	}
	//ボタンを乗せるフレームfb
	static void createfb(int w,int h) {
		fb.setBounds(w,h,200,115);
		fb.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fb.setUndecorated(true);
		fb.setBackground(new Color(0f, 0f, 0.2f, 0.5f));//赤・緑・青・透明度
		fb.setVisible(true);
		fb.setAlwaysOnTop(true);		//常に最前面に表示
    	//System.out.println("fbcreate");
	}
	public static void run(int w,int h) {
		fbi++;
		createfb(w,h);		//w,hは右クリックが押されたときの座標
		//System.out.println("fbrun");
	}
	public static JToggleButton getButton1() {
		return btClock;
	}
}
