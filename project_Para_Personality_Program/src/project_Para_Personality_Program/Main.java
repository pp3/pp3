package project_Para_Personality_Program;


import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

import function.Clock;
import function.Launcher;
import function.Save;

public class Main extends JPanel implements ActionListener{

		private static java.awt.GraphicsEnvironment env = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment();
		private static java.awt.DisplayMode displayMode = env.getDefaultScreenDevice().getDisplayMode();
	    //private static final int W = 1366;
	    //private static final int H = 768;
	    private static int W= displayMode.getWidth();
	    private static int H = displayMode.getHeight();
	    private static int mousex;
	    private static int mousey;

	    
	    private static final Font font =
	        new Font("Serif", Font.PLAIN, 20);

	    private final Date now = new Date();
	    private final Timer timer = new Timer(10, this);
	    
	    
	    private BufferedImage time;
	    private Graphics2D timeG;

	    private ImageControl ImageC = new ImageControl();
	    
	    private Clock cl;
	    public static boolean pt = true;
	    
	    private static float test = 0.005f;
	    private static boolean bl = true;
	    
	    public static Save save;
	    
	    
	    
	    public Main() {
	        super(true);
	        this.setPreferredSize(new Dimension(W, H));
	        timer.start();
	        	
	        Mediator tt = new Mediator(ImageC);
	        tt.start();
	        
	        cl = new Clock();
	        
	        
	        //CalendarBeta cB = new CalendarBeta();
	        	        

	    }

	    @Override
	    protected void paintComponent(Graphics g) {
	        Graphics2D g2d = (Graphics2D) g;
	        
	        //TG2D = g2d;
	        
	        
	        g2d.setRenderingHint(
	            RenderingHints.KEY_ANTIALIASING,
	            RenderingHints.VALUE_ANTIALIAS_ON);
	        int w = this.getWidth();
	        int h = this.getHeight();
	        g2d.setComposite(AlphaComposite.Clear);
	        g2d.fillRect(0, 0, w, h);
	        g2d.setComposite(AlphaComposite.Src);
	        g2d.setPaint(new Color(0,0,0,test));
	        g2d.fillRect(0, 0, w, h);
	        renderTime(g2d);
	        int w2 = time.getWidth() / 2;
	        int h2 = time.getHeight() / 2;
	        g2d.setComposite(AlphaComposite.SrcOver);
	        g2d.drawImage(ImageC.giveImageBack(), 0, 0, this);
	        g2d.drawImage(time, w / 2 - w2, h / 2 - h2, null);
	        g2d.drawImage(ImageC.giveImageChar(), 1250, 680, this);
	        
	        g2d.drawImage(ImageC.giveImageWeather(), 100, 100, this);
	        
	    }

	    private void renderTime(Graphics2D g2d) {
	        g2d.setFont(font);
	        int j = 0;
	        
	        
	        
	        String s = "hello";		//df.format(now);
	        //String s2 = "world";
	        FontMetrics fm = g2d.getFontMetrics();
	        int w = fm.stringWidth(s);
	        int h = fm.getHeight() * 2;
	        if (time == null && timeG == null) {
	            time = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
	            timeG = time.createGraphics();
	            timeG.setRenderingHint(
	                RenderingHints.KEY_ANTIALIASING,
	                RenderingHints.VALUE_ANTIALIAS_ON);
	            timeG.setFont(font);
	        }
	        timeG.setComposite(AlphaComposite.Clear);
	        timeG.fillRect(0, 0, w, h);
	        timeG.setComposite(AlphaComposite.Src);
	        timeG.setPaint(Color.magenta);
	        if(FrameButton.getButton1().isSelected()){
	        for(int i=0;i<3;i++){
	        	timeG.drawString(cl.stringClock(i),0,fm.getAscent()+j);
	        	j+=20;
	        }
	        }
	    }

	    private static void create() {
	        JFrame f = new JFrame();
	        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        f.setUndecorated(true);
	        f.setBackground(new Color(0f, 0f, 0f, 0.3f));

	        f.add(new Main());
	        //f.add(new Test_paint());
	        f.pack();
	        //f.setSize(500,500);
	        f.setLocation(0,0);
	        
	        f.setVisible(true);
	        if(pt == false){
	        	f.setAlwaysOnTop(true);
	        }
	        JPanel p = new JPanel();
	        p.addMouseListener(new MouseAdapter(){
	        	public void mouseClicked(MouseEvent e){
	            	//System.out.println("transme");
	        		mousex = e.getX();
	        		mousey = e.getY();
	        		if(e.getClickCount()==1){
	        			if(e.getButton() == 3 && pt == true){
	        				FrameButton.run(mousex,mousey);
	        				//Launcher.run(mousex, mousey);
	        				FrameButton fb = new FrameButton(mousex,mousey);
	        				fb.start();
	        				pt = false;
	        				//System.out.println("open");
	        			}
	        			else if(e.getButton() == 1 || e.getButton() == 2 || e.getButton() == 3 && pt == false){
	        				FrameButton.fb.dispose();
	        				Launcher.fl.dispose();
	        				pt = true;
	        				//System.out.println("close");
	        			}
	        		}
	        		else if(e.getClickCount() >= 2){
	        			if(e.getButton() == 1){
	        				Launcher.run(mousex,mousey);
	            			Launcher fl = new Launcher();
	            			fl.start();
	        			}
	    			}
	        		//System.out.println("transfb");
	        	}
	        });
	        f.getContentPane().add(p, BorderLayout.CENTER);
	        p.addKeyListener(new KeyListener(){
	    	    public void keyPressed(KeyEvent e){
	  	    	  int mod = e.getModifiersEx();
	  	    	  
	  	    	  //Shift + Ctrl で背景の透明度を変えて、透明部分のクリックの可否を変更する
	  	    	  if ((mod & InputEvent.SHIFT_DOWN_MASK) != 0 && (mod & InputEvent.CTRL_DOWN_MASK) != 0 && bl == true){
	  	    		  test = 0f;
	  	    		  bl = false;
	  	    	  }else if ((mod & InputEvent.SHIFT_DOWN_MASK) != 0 && (mod & InputEvent.CTRL_DOWN_MASK) != 0 && bl == false){
	  	    		  test = 0.005f;
	  	    		  bl = true;
	  	    	  }
	  	    }
				public void keyTyped(KeyEvent e) {}
				public void keyReleased(KeyEvent e) {}
	        });
	    }
	    
	    @Override
	    public void actionPerformed(ActionEvent e) {
	        now.setTime(System.currentTimeMillis());
	        this.repaint();
	    }

	    public static void main(String[] args) {
	    	
	    	save = new Save();		//インスタンス化。実体（本体）を作る
	    	//save.loadData();
	    	
	        EventQueue.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                create();
	            }
	        });
	        
	        //終了時に必ず行いたい処理を書く
	        Thread hook = new Thread(){
	        	public void run(){
	        		
	        		save.saveDataAll();			//全データの保存
	        	}
	        };
	        

	        Runtime.getRuntime().addShutdownHook(hook);
	        
	    }
}