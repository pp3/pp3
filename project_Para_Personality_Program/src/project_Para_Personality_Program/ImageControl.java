package project_Para_Personality_Program;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import weather_Enum.PlaceEnum;
import function.Weather;

public class ImageControl extends JPanel{

	private Toolkit tk = Toolkit.getDefaultToolkit();		//getImageを行うためのオブジェクト
	
	//それぞれキャラクタと背景が置いてあるフォルダのアドレス
	private String charaFileName = "graphics/chara/";
	private String backgroundFileName = "graphics/background/";
	
	private Image[] img = new Image[3];			//アニメーション用の画像を格納する配列
	private BufferedImage bufferImg;
	private BufferedImage[] bfImg = new BufferedImage[12];
	private Image backgroundImg;					//背景の画像を格納するオブジェクト
	private Image[] backImg = new Image[11];
	
	private int img_num;			//アニメーションのための要素番号を指定する整数
	private int back_num;			//背景アニメーション用

	
	//private Image WeatherImg;					//天気の画像を格納する変数
	private boolean weatherSettingFlag = true;
	private Weather weather = new Weather();
	
	
	public ImageControl(){
		loadImage();
	}
	
	
	public void loadImage(){
		
		for(int i = 0; i < 3; i ++){
			img[i] = tk.getImage(charaFileName + "walk" + (i+1) + ".png");
		}
		
		try {
			bufferImg = ImageIO.read(new File(charaFileName + "Actor4_5.png"));
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		

		for(int i = 0; i < 11; i++){
			backImg[i] = tk.getImage(backgroundFileName + "back" + (i+1) + ".png");
		}
		
		for(int y = 0; y < 4; y++){
			for(int x = 0; x < 3; x++){
				bfImg[x + y * 3] = bufferImg.getSubimage(32 * x, 32 * y, 32, 32);
			}
		}
		
		backgroundImg = tk.getImage(backgroundFileName + "back2.png");

		
	}
	
	//描画先に渡す画像の要素番号を受け取る
	public void catchImage(int catchImage_num){
		img_num = catchImage_num;		//
	}
	
	
	//描画先に渡す画像の要素番号を受け取る（背景）
	public void catchImageBack(int catchImage_num){
		back_num = catchImage_num;
	}
	
	
	//描画先にImageを渡す：キャラクター用
	public Image giveImageChar(){
		return bfImg[img_num + 3];
	}
	
	//描画先にImageを渡す：背景画像用
	public Image giveImageBack(){
		
		if(backgroundImg == null){
			System.out.println("ERROR");
			System.exit(1);
		}
		
		return backImg[back_num];
	}
	
	
	
	public Image giveImageWeather(){

		if(weatherSettingFlag){
	        PlaceEnum pe = PlaceEnum.TOKYO;
	        
	        weather.placeCatch(pe);
	        weather.weatherScript();
	        weatherSettingFlag = false;
		}
        //System.out.println(backgroundFileName + "weather/" + weather.weatherState().name() + ".png");
        
        return tk.getImage(backgroundFileName + "weather/" + weather.weatherState().name() + ".png");
        
	}
	
	
}
