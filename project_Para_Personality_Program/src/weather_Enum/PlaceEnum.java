package weather_Enum;


public enum PlaceEnum {

	
	//北海道
		//道北
	WAKKANAI(1100),
	ASAHIKAWA(1200),
	RUMOI(1300),
	
		//道東
	ABASHIRI(1710),
	KITAMI(1720),
	MONBETU(1730),
	NEMURO(1800),
	KUSHIRO(1900),
	OBIHIRO(2000),
	
		//道央
	SAPPORO(1400),
	IWAMIZAWA(1500),
	GUCCYAN(1600),
	
		//道南
	MURORANN(2100),
	URAGA(2200),
	HAKODATE(2300),
	ESASHI(2400),
	
	
	//東北
		//青森県
	AOMORI(3110),
	MUTU(3120),
	YATO(3130),
	
		//岩手県
	MORIOKA(3310),
	MIYAKO(3320),
	OOHUNATO(3330),
	
		//宮城県
	SENNDAI(3410),
	SIRAISHI(3420),
	
		//秋田県
	AKITA(3210),
	YOKOTE(3220),
	
		//山形県
	YAMAGATA(3510),
	KOMEAWA(3520),
	SAKATA(3530),
	SINNJO(3540),
	
		//福島県
	FUKUSHIMA(3610),
	ONAHAMA(3620),
	WAKAMATU(3630),
	
	
	//関東
		//東京都
	TOKYO(4410),
	OOSHIMA(4420),
	HACHIJOJIMA(4430),
	HUSHIMA(4440),
	
		//神奈川県
	YOKOHAMA(4610),
	ODAWARA(4620),
	
		//埼玉県
	SAITAMA(4310),
	KUMAGAYA(4320),
	CHICHIBU(4330),
	
		//千葉県
	CHIBA(4510),
	CYOSHI(4520),
	TATEYAMA(4530),
	
		//茨城県
	MITO(4010),
	TUCHIURA(4020),
	
		//栃木県
	UTUNOMIYA(4110),
	OODAWARA(4120),
	
		//群馬県
	MAEBASHI(4210),
	MINAKAMI(4220),
	
		//山梨県
	KOHU(4910),
	KAWAGUCHIKO(4920),
	
	
	//信越
		//新潟県
	NIGATA(5410),
	NAGAOKA(5420),
	TAKADA(5430),
	AIKAWA(5440),
	
		//長野県
	NAGANO(4810),
	MATUMOTO(4820),
	IIDA(4830),
	
	
	//北陸
		//富山県
	TOYAMA(5510),
	FUSEGI(5520),
	
		//石川県
	KANAZAWA(5610),
	WAJIMA(5620),
	
		//福井県
	FUKUI(5710),
	TURUGA(5720),
	
	
	//東海
		//愛知県
	NAGOYA(5110),
	TOYOSHI(5120),
	
		//岐阜県
	GIFU(5210),
	TAKAYAMA(5220),
	
		//静岡県
	SIZUOKA(5010),
	AJIRO(5020),
	MISHIMA(5030),
	HAMAMATU(5040),
	
		//三重県
	TSU(5310),
	OWASE(5320),
	
	
	//近畿
		//大阪府
	OSAKA(6200),
	
		//兵庫県
	KOBE(6310),
	TOYOOKA(6320),
	
		//京都府
	MAIDURU(6120),
	KYOTO(6110),
	
		//滋賀県
	OOTSU(6010),
	HIKONE(6020),
	
		//奈良県
	NARA(6410),
	KAZEYA(6420),
	
		//和歌山県
	WAKAYAMA(6510),
	SHIONOMISAKI(6520),
	
		//鳥取県
	TOTTORI(6910),
	YONAGO(6920),
	
		//島根県
	MATUE(6810),
	HAMADA(6820),
	SAIGO(6830),
	
		//岡山県
	OKAYMA(6610),
	TUYAMA(6620),
	
		//広島県
	HIROSHIMA(6710),
	ATUHARA(6720),
	
		//山口県
	SHIMONOSEKI(8110),
	YAMAGUCHI(8120),
	YANAI(8130),
	OGI(8140),
	
	
	//四国
		//徳島県
	TOKUSIMA(7110),
	HIWASA(7120),
	
		//香川県
	TAKAMATU(7200),
	
		//愛媛県
	MATUYAMA(7310),
	NIIHAMASHI(7320),
	UWAJIMA(7330),
	
		//高知県
	KOCHI(7410),
	MUROTO(7420),
	SHIMIZU(7430),
	
	
	//九州
		//福岡県
	FUKUOKA(8210),
	HACHIMAN(8220),
	IIDUKA(8230),
	KURUME(8240),
	
		//佐賀県
	SAGA(8510),
	IMARI(8520),
	
		//長崎県
	NAGASAKI(8410),
	SASEBO(8420),
	IDUHARA(8430),
	FUKUE(8440),
	
		//熊本県
	KUMAMOTO(8610),
	ASO(8620),
	USHIBUKA(8630),
	HITOYOSHI(8640),
	
		//大分県
	OITA(8310),
	NAKATU(8320),
	HIDA(8330),
	SAHAKU(8340),
	
		//宮崎県
	MIYAZAKI(8710),
	NOBEOKA(8720),
	MIYAKONOJO(8730),
	TAKACHIHO(8740),
	
		//鹿児島県
	NASE(1000),
	KAGOSHIMA(8810),
	KANOYA(8820),
	TANEGASHIMA(8830),
	
	
	//沖縄県
	NAHA(9110),
	NAGO(9120),
	KUMEJIMA(9130),
	MINAMIDAITO(9200),
	MIYAKOJIMA(9300),
	ISHIGAKIJIMA(9410),
	YONAGUNISHIMA(9420);
	
	
	private int value;
	
	private PlaceEnum(int n){
		this.value = n;
	}
	
	public int getNum(){
		return this.value;
	}
	
	
}
